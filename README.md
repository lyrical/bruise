# What

Bruise is a sourceable shell script that lets you automatically configure, build, and load environments for your tree.  The idea is to make virtualevns even lazier than they are. To install, simply _source /path/to/your/bruise.sh_ in your .bashrc (or flavour).

# How

Bruise overrides the shell's *cd* command.  When changing into a directory bruise tries to determine the branch, and whether or not a virtualenv exists/needs to exist, based on the presence at the **top-level of the repo** of a requirements.txt.

Your commands are  *bruise* (probably not needed), *bruisemake*, *bruisedelete*, and *bruiselist*. Play!

# Why

Ultimately, I just wanted the ability to cd into a directory, and activate my environment. No thinking required. Juggling virtualenvs between branches, repos, and projects is far too much of a pain. Bruise makes that go away.

# What Else

Think about adding [pyenv](https://github.com/yyuu/pyenv) to your universe. The combination of bruise and pyenv has made my life a great one.

In the future, I'll restore support for rvm and nodejs, as needed. Of course if you want to make that happen with a pull request, you're welcome to it.
